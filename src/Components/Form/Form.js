import React from 'react';
import {useForm} from "react-hook-form";
import './Form.css';
import '../../bootstrap.min.css';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import {withRouter} from 'react-router-dom';


const schema = yup.object().shape({
    name : yup.string("name must be string").required("enter your name").min(2),
    lastname: yup.string("lastname must be string").required("enter your lastname").min(5,"enter more "),
    age: yup.number("Age must be number ").required("enter your age"),
    email: yup.string().email("email is invalid")
});


function Form(props) {
    const{register, handleSubmit , watch , formState:{errors}} = useForm({
        resolver: yupResolver(schema)
     });
    return (
        <form  className="firstform"   onSubmit={handleSubmit((data)=>{
            props.setdata({islog:true,data:data})
            props.history.push({pathname : 'todo'});
        })
            
            
            }>
            <h5>Enter your information</h5>
            <div className="form-group my">
                <label>name</label>
                <input className="form-control" {...register("name")}/>
                <p>{errors.name?.message}  </p>
            </div>
            <div className="form-group my">
                <label>lastname</label>
                <input className="form-control" {...register("lastname")} />
                <p>{errors.lastname?.message}</p>
            </div>

            <div className="form-group my">
                <label>age</label>
                <input className="form-control" type="number" {...register("age")}/>
                <p>{errors.age?.message}</p>                
            </div>

            <div className="form-group my">
                <label>email</label>
                <input className="form-control" {...register("email")}/>
                <p>{errors.email?.message}</p>
            </div>

             <input  className="mt-4 form-control btn btn-danger" type="submit"/>
             

        </form>

    )
}

export default withRouter(Form);