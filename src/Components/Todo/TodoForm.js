import React, {useState} from 'react'


function TodoForm(props) {

    const[input, setinput] = useState(props.edit ? props.edit.value : '');

    const handlechange = e => {
        setinput(e.target.value);
    }

    const handleSubmit= e => {
        e.preventDefault();
        props.onSubmit({
            id : Math.floor(Math.random()*10000),
            text : input
        })
        setinput('')
    }

    return (
        <form className="todo-form" onSubmit={handleSubmit}>
            {props.edit ? (
            <>
            <input type="text" name="text" onChange={handlechange}  value={input} className="todo-input edit"/>
            <button className="todo-button edit">update</button>
            </>)
            : (
            <>
            <input type="text" name="text" onChange={handlechange}  value={input} className="todo-input"/>
            <button className="todo-button">add todo</button>
            </>
            )
            }
        </form>
    )
}

export default TodoForm
