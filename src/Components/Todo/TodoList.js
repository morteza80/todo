import React,{useState} from 'react'
import { TiDownloadOutline } from 'react-icons/ti';
import TodoForm from './TodoForm'
import Todo from './Todo'
import {BsPersonBoundingBox} from 'react-icons/bs'

function TodoList(props) {
    const[todos,setTodos]=useState([]);

    const addTodo = todo => {
        if(!todo.text || /^\s*$/.test(todo.text))
        {
            return;
        }
        const newTodo = [todo,...todos];
        setTodos(newTodo);
    }


    const updateTodo = (todoid, newvalue) => {
        if(!newvalue.text || /^\s*$/.test(newvalue.text))
        {
            return;
        }
        setTodos(prev => prev.map(item => (item.id ===todoid ? newvalue : item)));
    }


    const removeTodo = id =>{
        const newt= [...todos].filter(todo => todo.id !== id);
        setTodos(newt)
    }

    const completeTodo = id =>{
        let newtodos = todos.map(todo => {
            if(todo.id===id){
                todo.isComplete = !todo.isComplete;
            }
            return todo
        })
        setTodos(newtodos);
    }

    return (
        <>
            <div className="imdiv">
                <div className="todo-topbar">
                    <h1>hi {props.state.data.name}, whats your plan?</h1>
                    <BsPersonBoundingBox className="pricon" onClick={props.changeshow}/>
                </div>
                <TodoForm onSubmit={addTodo}/>
                <Todo todos={todos} completeTodo={completeTodo} removeTodo={removeTodo} updateTodo={updateTodo}/>
            </div>
        </>
    )
}

export default TodoList
