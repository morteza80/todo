import React from 'react';
import Aux from './hoc/Aux1'
import Form from './Components/Form/Form'
import {useState} from 'react';
import TodoForm from './Components/Todo/TodoForm';
import TodoList from './Components/Todo/TodoList';
import './App.css'
import {Redirect, Route} from 'react-router-dom'

function App() {
  const [state , setState] = useState({
    islog : false,
    data : '',
    showpr: false
  })

  const changeshow = () => (
    setState ((prevstate) =>
    {
      return {...prevstate , showpr : !state.showpr}
    }
    
    
    )
  )

  return (
    /*<div className="todo-app">
      {state.islog? 
      <TodoList/>
      :
      <Form setdata={setState} />
      }

    </div>*/
      <>
        <div className="todo-app">
          <Route path="/" exact={true} render={ () => ( <Form setdata={setState} />)}  />
          <Route path="/todo" render={ () => ( <TodoList state={state} changeshow={changeshow}/>)} />
        </div>
        {state.showpr ? (
          <div className="modal2" onClick = {changeshow}>
              <div className="information" >
                  <h1>name : {state.data.name}</h1>
                  <h1>lastname : {state.data.lastname}</h1>
                  <h1>age : {state.data.age}</h1>
                  <h1>email : {state.data.email}</h1>
              </div>
          </div>
        )
        : (
          <></>
        )}

    </>
  );
}

export default App;
